from matplotlib.patches import Circle

import matplotlib.pyplot as plt
import pandas as pd
import random


def initconfig(sensor, numnodes, network, locsize):
    for x in range(numnodes):
        sensor["name"] = str(x)
        sensor["xloc"] = random.randint(1, locsize)
        sensor["yloc"] = random.randint(1, locsize)
        network = network.append(sensor, ignore_index=True)
    return network


def plotnodes(network):
    network.plot(kind='scatter', x='xloc', y='yloc',
                 c='membership', colormap='viridis', marker='8', s=20)
    for index, row in network.iterrows():
        plt.text(row["xloc"]+0.3, row["yloc"] +
                 0.3, row["name"], fontsize=6)
    plt.draw()
