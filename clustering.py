from matplotlib.patches import Circle

import matplotlib.pyplot as plt
import pandas as pd


def scannodes(network):
    for index, row in network.iterrows():
        n = []
        range = Circle((row['xloc'], row['yloc']), radius=row['range'])
        for i, r in network.iterrows():
            if ((range.contains_point([r['xloc'], r['yloc']])) & (i != index)):
                n.append(i)
        network.at[index, 'neighbour'] = n
    return network


def scanhop(numnodes, network):
    # index -> node acuan
    # notnbr -> array yang bukan node tetangga index
    # temp_nbrofnot -> array temp untuk nyimpen tetangga dari anggota notnbr
    for index, row in network.iterrows():
        neighbour = network.at[index, 'neighbour']
        notnbr = []
        temp_hop = []
        hop = {1: neighbour}
        network.at[index, 'hop'] = hop
        for i in range(numnodes):
            if (i not in neighbour) & (i != index):
                notnbr.append(i)

        for j in range(len(notnbr)):
            temp_nbrofnot = network.at[notnbr[j], 'neighbour']
            for k in range(len(neighbour)):
                if (neighbour[k] in temp_nbrofnot):
                    temp_hop.append(notnbr[j])
                    # print index, 'jaraknya 2 hop gan via', neighbour[k], 'ke', notnbr[j]
        hops = {2: temp_hop}
        hop.update(hops)
        network.at[index, 'hop'] = hop
    return network


def searchmax(nodesmax, network):
    maxhop = 0
    i = 0
    for index, row in network.iterrows():
        if (index not in nodesmax) & (network.at[index, 'membership'] == 0):
            hop = network.at[index, 'hop']
            count = len(hop[1]) + len(hop[2])
            if count == 0:
                continue
            else:
                if maxhop < count:
                    maxhop = count
                    i = index
    return i, maxhop


def setmembership(numnodes, network):
    nodesmax = []
    for index, row in network.iterrows():
        node = searchmax(nodesmax, network)
        if not ((node[0] == 0) & (node[1] == 0)):
            network.at[node[0], 'membership'] = 2
        member = network.at[node[0], 'hop']
        for i in member:
            for j in member[i]:
                network.at[j, 'membership'] = 1
                network.at[j, 'head'] = node[0]
    return network
