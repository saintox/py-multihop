from initialization import *
from clustering import *

import random
import numpy as np
import matplotlib.pyplot as plt


# main variables
numnodes = 50
locsize = 50
maxcluster = 5
maxrange = 5
network = pd.DataFrame()
sensor = dict(
    name=None,
    xloc=0,
    yloc=0,
    range=maxrange,
    hop=dict(),
    neighbour=[],
    membership=0,
    head=0
)

network = initconfig(sensor, numnodes, network, locsize)
# first phase of clustering
scannodes(network)
scanhop(numnodes, network)
setmembership(numnodes, network)
plotnodes(network)


print(network)
plt.show()
